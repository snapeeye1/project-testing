##React courses, first project: testing.##
 
**Requirements:**

 - Sign in and login via server
 
 Implementation status: Done (with bug).
 
 Also offline user sign in is available (just push sign in button to enter as Tester user).
 
 - Anonymous mode
 
 Implementation status: Done.
 
 - All the progress is saved (doesn't in anonymous mode)
 
 Implementation status: Done.
 
 - Localization (en, ru)
 
 Implementation status: Done.
 
 - Hint mode
 
 Implementation status: Done.
 
 - Ability to terminate session
 
 Implementation status: Done.
 
 - Footer log in status and question counter
 
 Implementation status: Done.
 
 - Nickname in header and setting modal
 
 Implementation status: Done.
 
 - Navigation bar for questions
 
 Implementation status: Done.
 
 **Total status: Done 9/9, Open: 0/9.**