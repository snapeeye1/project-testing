import http from 'http';
import url from'url';
import mysql from 'mysql';
import hash from "hash.js";
// import qs from'querystring';

/////////////////////////////////////CONSTANTS//////////////////////////////////////
const postAvailable = {
    Login: /\/\b(\w*login\w*)\b/g,
    Registration: /\/\b(\w*registration\w*)\b/g,
};
// TODO remove old SQL service config after verifying work of the new better one.
// https://www.db4free.net
// const sqlConfig = {
//     host: "db4free.net",
//     user: "mysqluser123",
//     password: "mysqlpass",
//     database: 'project0testing',
// };
const sqlConfig = {
    host: "sql2.freemysqlhosting.net",
    user: "sql2271071",
    password: "wC8!bF4*",
    database: 'sql2271071',
};
// https://www.freemysqlhosting.net/account/
////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////POST PROCESSORS/////////////////////////////////////
const processLoginRequest = (data, response) => {
    console.log('LOGIN PROCESSOR');
    const con = mysql.createConnection(sqlConfig);

    con.connect((err) => {
        if (err) throw err;
        console.log("Connected to Database!");

        const password = hash.sha256().update(data.password).digest('hex');
        const sql = `SELECT username FROM users WHERE email='` + data.email + `' AND password='` + password + `'`;
        con.query(sql, (err, result) => {
            if (err) throw err;
            if (result[0] && result[0].username) {
                console.log('LOGIN SUCCESS');
                response.writeHead(200, {'Content-Type': 'application/json', 'Access-Control-Allow-Origin': '*'});
                const payload = JSON.stringify({
                    data: result[0].username,
                    status: 'serverLoginSuccess',
                });
                response.end(payload);
            } else {
                console.log('LOGIN ERROR');
                response.writeHead(400, {'Content-Type': 'application/json', 'Access-Control-Allow-Origin': '*'});
                const payload = JSON.stringify({
                    data: 'Data not found or damaged.',
                    status: 'serverLoginError',
                });
                response.end(payload);
            }
        });
    });
};
global.processLoginRequest = processLoginRequest;

const processRegistrationRequest = (data, response) => {
    console.log('REGISTRATION PROCESSOR');
    const con = mysql.createConnection(sqlConfig);

    con.connect((err) => {
        if (err) throw err;
        console.log("Connected to Database!");

        const sql = `SELECT username FROM users WHERE email='` + data.email + `'`;
        con.query(sql, (err, result) => {
            if (err) throw err;

            if (!result.length) {
                const password = hash.sha256().update(data.password).digest('hex');
                const regSql = `INSERT INTO users (email,password,username) VALUES ('` + data.email + `','` + password + `','` + data.username + `')`;

                con.query(regSql, (err, result) => {
                    if (err) throw err;

                    if (result.serverStatus === 2 && result.affectedRows === 1) {
                        response.writeHead(200, {'Content-Type': 'application/json', 'Access-Control-Allow-Origin': '*'});
                        const payload = JSON.stringify({
                            data: 'SUCCESS',
                            status: 'serverRegistrationSuccess',
                        });
                        response.end(payload);
                    } else {
                        response.writeHead(400, {'Content-Type': 'application/json', 'Access-Control-Allow-Origin': '*'});
                        const payload = JSON.stringify({
                            data: 'Registration error. Try again.',
                            status: 'serverRegistrationError',
                        });
                        response.end(payload);
                    }
                });
            } else {
                response.writeHead(400, {'Content-Type': 'application/json', 'Access-Control-Allow-Origin': '*'});
                const payload = JSON.stringify({
                    data: 'Account with such email is already existing!',
                    status: 'serverRegistrationDuplicate',
                });
                response.end(payload);
            }
        });
    });
};
global.processRegistrationRequest = processRegistrationRequest;
////////////////////////////////////////////////////////////////////////////////////

///////////////////////////////////HELPERS//////////////////////////////////////////
const getRequestTarget = (url, masks) => {
    for (const target in masks) {
        const regex = masks[target];

        if (new RegExp(regex).test(url)) {
            return target;
        }
    }

    return null;
};

const processPostRequest = (target, data, response) => {
    const targetProcessorName = 'process' + target + 'Request';
    console.log(targetProcessorName);
    global[targetProcessorName](data, response);
};
////////////////////////////////////////////////////////////////////////////////////

//////////////////////////////SERVER DEDICATION/////////////////////////////////////
http.createServer((req, res) => {
    console.log('Request processing...');
    if (req.method === 'POST') {
        console.log('Detected POST request...');
        const requestTarget = getRequestTarget(req.url, postAvailable);

        if (!requestTarget) {
            res.writeHead(400, {'Content-Type': 'text/plain'});
            res.end('Wrong target: ' + req.url);
        }

        let body = '';
        req.on('data', (data) => {
            body += data;

            // Too much POST data, kill the connection!
            // 1e6 === 1 * Math.pow(10, 6) === 1 * 1000000 ~~~ 1MB
            if (body.length > 1e6)
                request.connection.destroy();
        });
        req.on('end', () => {
            let data = {};

            try {
                data = JSON.parse(body);
            } catch (e) {
                res.writeHead(400, {'Content-Type': 'text/plain'});
                res.end('Failed to parse: ' + body);
            }

            processPostRequest(requestTarget, data, res);
        });
    } else if (req.method === 'GET') {
        console.log('Detected GET request...');
        const data = url.parse(req.url, true).query;
        res.end(JSON.stringify(data));
    }
}).listen(3000);
////////////////////////////////////////////////////////////////////////////////////