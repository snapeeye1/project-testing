import React from 'react';
import './index.less';
import { initialize, getCookie } from "../utils/helpers";
import TestingPage from "../Main/TestingPage/index.jsx";

const checkAuthorizedUser = () => {
    const lastUser = getCookie('lastUser');
    let user = null;

    try {
        if (lastUser) {
            user = JSON.parse(localStorage.getItem(lastUser));
        }
    } catch (e) {}

    if (!user || !user.username) {
        user = {
            username: null,
            isHintModeEnabled: false,
            isTestingStarted: false,
            questions: null,
            currentQuestionIndex: 0,
            isTestCompleted: false,
        };
    }

    return { ...user };
};

initialize(TestingPage, checkAuthorizedUser);