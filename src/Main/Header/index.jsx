import React from 'react';
import './Header.less';
import Modal from 'react-modal';
import Toggle from 'react-toggle';
import "react-toggle/style.css";
import ReactDropdown from "react-dropdown";

export default class Header extends React.PureComponent {
    render() {
        const { lang, resources, langOptions, onLangSelect, isHintModeEnabled, onChangeHintModeState, username,isSettingsModalOpen, onChangeSettingsModalState, onTerminateSession, isTestCompleted, onCompleteTest, testResult } = this.props;
        const customStyles = {
            content : {
                top                   : '30%',
                left                  : '40%',
                right                 : '40%',
                bottom                : '30%',
            }
        };
        const testCompleteButtonActiveClass = isTestCompleted && !testResult ? ' active' : '';

        return (
            <div className={'page-wrapper__ header header'}>
                {
                    isTestCompleted && !testResult ?
                        <div className={`header__complete-button complete-button${ testCompleteButtonActiveClass }`}>
                            <button className={'complete-button__button'} onClick={ onCompleteTest }>{ resources.mainCompleteButtonText }</button>
                        </div>
                        :
                        <div className={'header__stub'} />
                }
                <div className={'header__username'}>{ username ? username : resources.mainAnonymousUserNameText }</div>
                <div className={'header__settings'} onClick={ onChangeSettingsModalState } />
                <Modal
                    isOpen = { isSettingsModalOpen }
                    onRequestClose = { onChangeSettingsModalState }
                    style = { customStyles }
                    ariaHideApp = { false }
                >
                    <div className={'settings-wrapper'}>
                        <div className={'settings-wrapper__settings-header'}>{ resources.mainSettingsModalHeaderText }</div>
                        <div className={'settings-wrapper__settings-body'}>
                            <div className={'settings-body__hint-mode hint-mode'}>
                                <span className={'hint-mode__text'}>{ resources.mainHintModeToggleText }</span>
                                <Toggle
                                    className={'hint-mode__toggle'}
                                    checked={ isHintModeEnabled }
                                    onChange={ onChangeHintModeState } />
                            </div>
                            <div className={'settings-body__lang-dropdown lang-dropdown'}>
                                <span className={'lang-dropdown__text'}>{ resources.mainLanguageDropdownText }</span>
                                <div className={'lang-dropdown__dropdown'}>
                                    <ReactDropdown
                                        className = 'lang-dropdown'
                                        controlClassName = 'lang-dropdown-control'
                                        placeholderClassName='lang-dropdown-placeholder'
                                        menuClassName = 'lang-dropdown-menu'
                                        options = { langOptions }
                                        onChange = { onLangSelect }
                                        value={ lang } />
                                </div>
                            </div>
                            <div className={'settings-body__terminate-session terminate-session'}>
                                <span className={'terminate-session__text'}>{ resources.mainTerminateSessionText }</span>
                                <button className={'terminate-session__button'} onClick={ onTerminateSession }>{ resources.mainTerminateSessionButtonText }</button>
                            </div>
                        </div>
                        <div className={'settings-wrapper__settings-footer settings-footer'}>
                            <button className={'settings-footer__close-button'} onClick={ onChangeSettingsModalState }>{ resources.mainSettingsModalCloseButtonText }</button>
                        </div>
                    </div>
                </Modal>
            </div>
        );
    }
}