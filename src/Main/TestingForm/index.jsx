import React from 'react';
import './TestingForm.less';

export default class TestingForm extends React.PureComponent {
    onElementClick = (event) => {
        const { onChangeCurrentQuestionIndex } = this.props;
        onChangeCurrentQuestionIndex(parseInt(event.target.textContent) - 1);
    };

    onOptionClick = (event) => {
        const { currentQuestionIndex, onUserSelectQuestionOption } = this.props;
        const optionId = parseInt(event.target.id);
        onUserSelectQuestionOption(currentQuestionIndex, optionId);
    };
    
    render() {
        const { onElementClick, onOptionClick } = this;
        const { questions, currentQuestionIndex, isHintModeEnabled, currentQuestionUserSelectId } = this.props;
        const selectedQuestion = questions && questions[currentQuestionIndex];
        const tipHiddenClass = isHintModeEnabled ? '' : ' hidden';

        return (
            <React.Fragment>
                <div className={'testing-form__navigation navigation'}>
                    {
                        questions && questions.map((value, index) => {
                            const elementIndex = index + 1;
                            const elementActiveClass = currentQuestionIndex === elementIndex - 1 ? ' active' : '';
                            return <div key={ index } onClick={ onElementClick } className={`navigation__element${ elementActiveClass }`}>{ elementIndex }</div>
                        })
                    }
                </div>
                <div className={'testing-form__question-block question-block'}>
                    <div className={'question-block__question'}>{ selectedQuestion && selectedQuestion.question }</div>
                    <div className={`question-block__tip${ tipHiddenClass }`}>
                        <div className={'tip__icon'} />
                        <span className={'tip__text'} >{ selectedQuestion && selectedQuestion.tip }</span>
                    </div>
                    <div className={'question-block__options options'}>
                        {
                            selectedQuestion && selectedQuestion.options.map((value, index) => {
                                const optionActiveClass = index + 1 === currentQuestionUserSelectId ? ' active' : '';
                                return <div key={ value.id } id={ value.id } className={`options___option${ optionActiveClass }`} onClick={ onOptionClick }>{ value.text }</div>
                            })
                        }
                    </div>
                </div>
            </React.Fragment>
        );
    }
}