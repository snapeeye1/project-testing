import React from 'react';
import './Footer.less';

export default class Footer extends React.PureComponent {
    render() {
        const { resources, username, questionsNumber, currentQuestionIndex } = this.props;

        return (
            <div className={'page-wrapper__footer footer'}>
                {
                    username ?
                        <React.Fragment>
                            <div className={'footer__user-info'}>{ resources.mainUserInfoText }</div>
                            <div className={'footer__question-counter'}>
                                { resources.mainQuestionCounterText + ' ' + (currentQuestionIndex + 1) + '/' + questionsNumber }
                            </div>
                        </React.Fragment>
                        :
                        <div className={'footer-anonymous-info'}>{ resources.mainAnonymousInfoText }</div>
                }
            </div>
        );
    }
}