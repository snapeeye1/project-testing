import React from 'react';
import './TestingPage.less';
import Header from '../Header/index.jsx';
import Footer from '../Footer/index.jsx';
import TestingForm from '../TestingForm/index.jsx';
import questions from './../../data/data';

export default class TestingPage extends React.Component {
    state = {
        lang: this.props.lang,
        isTestingStarted: this.props.isTestingStarted,
        isHintModeEnabled: this.props.isHintModeEnabled,
        isSettingsModalOpen: false,
        questions: this.props.questions,
        currentQuestionIndex: this.props.currentQuestionIndex,
        isTestCompleted: this.props.isTestCompleted,
        testResult: this.props.testResult,
    };

    componentDidMount() {
        this.setState({
            questions: this.importQuestionsData(questions, null, true),
        });
    }

    componentDidUpdate() {
        this.saveUserData();
    }

    onLangSelect = (selectedValue) => {
        const selectedLang = selectedValue.value;
        document.cookie = "lastLang=" + selectedLang;
        this.setState({
            lang: selectedLang,
            questions: this.importQuestionsData(questions, selectedLang),
        });
    };

    onChangeTestingState = () => {
        this.setState(() => ({
            isTestingStarted: true
        }));
    };

    onChangeHintModeState = () => {
        this.setState(prevState => ({
            isHintModeEnabled: !prevState.isHintModeEnabled,
        }));
    };

    onChangeSettingsModalState = () => {
        this.setState(prevState => ({
            isSettingsModalOpen: !prevState.isSettingsModalOpen,
        }));
    };

    onUserSelectQuestionOption = (questionIndex, optionId) => {
        const { questions } = this.state;
        const updatedQuestions = [ ...questions ];
        updatedQuestions[questionIndex].userSelectId = optionId;
        const isTestCompleted = this.checkIsTestCompleted();
        this.setState({
            questions: [ ...updatedQuestions ],
            isTestCompleted: isTestCompleted,
        });
    };

    saveUserData = () => {
        const { isHintModeEnabled, isTestingStarted, questions, currentQuestionIndex, isTestCompleted, testResult } = this.state;
        const { username } = this.props;
        let user = {};

        try {
            if (username) {
                user = JSON.parse(localStorage.getItem(username));
            } else {
                return;
            }
        } catch(e) {
            window.location.href = '/login.html';
        }

        user.isHintModeEnabled = isHintModeEnabled;
        user.isTestingStarted = isTestingStarted;
        user.questions = [ ...questions ];
        user.currentQuestionIndex = currentQuestionIndex;
        user.isTestCompleted = isTestCompleted;
        user.testResult = testResult;
        localStorage.setItem(username, JSON.stringify(user));
    };

    importQuestionsData = (importQuestions, selectedLang, isFirstLoad) => {
        const { questions, lang } = this.state;
        const newLang = selectedLang || lang;
        const loadedQuestions = isFirstLoad ? this.props.questions : null;
        return loadedQuestions || importQuestions.reduce((previousValue, currentItem, index) => {
            const savedUserSelectId = questions && questions[index] && questions[index].userSelectId;
            const question = { ...currentItem[newLang], userSelectId: savedUserSelectId || 0 };
            previousValue.push(question);
            return previousValue;
        }, []);
    };

    onChangeCurrentQuestionIndex = (index) => {
        this.setState({ currentQuestionIndex: index });
    };

    onTerminateSession = () => {
        const { username } = this.props;

        if (username) {
            document.cookie = 'lastUser=';
            localStorage.removeItem(username);
        }

        window.location.href = '/login.html';
    };

    checkIsTestCompleted = () => {
        const {questions} = this.state;

        return questions.every((element) => {
            if (element.userSelectId > 0) {
                return true;
            }

            return false;
        });
    };

    onCompleteTest = () => {
        const { questions } = this.state;
        const correctAnswerPoints = 10;

        const result = questions.reduce((previousValue, currentItem) => {
            if (currentItem.userSelectId === currentItem.correctId) {
                previousValue += correctAnswerPoints;
            }

            return previousValue;
        }, 0);

        this.setState({
            testResult: this.getTestResultString(result),
        });
    };

    getTestResultString = (result) => {
        if (!result && result !== 0) {
            return 'ERROR ERROR';
        }

        const { resources } = this.props;
        const { lang } = this.state;
        const langResources = resources[lang];
        const localizationString = 'mainTestResult' + result;
        return langResources[localizationString];
    };

    render() {
        const { onLangSelect, onChangeTestingState, onChangeHintModeState, onChangeSettingsModalState, onChangeCurrentQuestionIndex, onUserSelectQuestionOption, onTerminateSession, onCompleteTest } = this;
        const { lang, isTestingStarted, isHintModeEnabled, isSettingsModalOpen, questions, currentQuestionIndex, isTestCompleted, testResult } = this.state;
        const { langOptions, resources, username } = this.props;
        const langResources = resources[lang];
        const currentQuestionUserSelectId = questions && questions[currentQuestionIndex].userSelectId;

        return (
            <div className={'page-wrapper'}>
                <Header
                    lang = { lang }
                    resources = { langResources }
                    langOptions = { langOptions }
                    onLangSelect = { onLangSelect }
                    isHintModeEnabled = { isHintModeEnabled }
                    onChangeHintModeState = { onChangeHintModeState }
                    username = { username }
                    isSettingsModalOpen = { isSettingsModalOpen }
                    onChangeSettingsModalState = { onChangeSettingsModalState }
                    onTerminateSession = { onTerminateSession }
                    isTestCompleted = { isTestCompleted }
                    onCompleteTest = { onCompleteTest }
                    testResult = { testResult } />
                {
                    !isTestingStarted ?
                        <div className={'page-wrapper__start-block start-block'}>
                            <div className={'start-block__text'}>{ langResources.mainStartTestingText }</div>
                            <button className={'start-block__button'} onClick={ onChangeTestingState }>{ langResources.mainStartTestingButtonText }</button>
                        </div>
                        : isTestingStarted && !testResult ?
                        <div className={'page-wrapper__testing-form testing-form'}>
                            <TestingForm
                                questions = { questions }
                                currentQuestionIndex = { currentQuestionIndex }
                                onChangeCurrentQuestionIndex = { onChangeCurrentQuestionIndex }
                                onUserSelectQuestionOption = { onUserSelectQuestionOption }
                                isHintModeEnabled = { isHintModeEnabled }
                                currentQuestionUserSelectId = { currentQuestionUserSelectId } />
                        </div>
                        :
                        <div className={'page-wrapper__test-result test-result'}>
                            <div className={'test-result__title'}>{ testResult.split('#')[0] }</div>
                            <div className={'test-result__text'}>{ testResult.split('#')[1] }</div>
                        </div>
                }
                <Footer
                    resources = { langResources }
                    username = { username }
                    questionsNumber = { questions && questions.length }
                    currentQuestionIndex = { currentQuestionIndex } />
            </div>
        );
    }
}