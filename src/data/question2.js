const question = {
    en: {
        question: 'What DOM does React.js interact with?',
        tip: 'React uses its own HOME to display its components.',
        correctId: 3,
        options: [
            {
                id: 1,
                text: 'Shadow DOM.',
            },
            {
                id: 2,
                text: 'Native DOM.',
            },
            {
                id: 3,
                text: 'Virtual DOM.',
            },
            {
                id: 4,
                text: 'Dynamic DOM.',
            },
        ],
    },
    ru: {
        question: 'С каким DOM взаимодействует React.js?',
        tip: 'React использует собсвенный DOM для отображения своих компанентов.',
        correctId: 3,
        options: [
            {
                id: 1,
                text: 'Shadow DOM.',
            },
            {
                id: 2,
                text: 'Native DOM.',
            },
            {
                id: 3,
                text: 'Virtual DOM.',
            },
            {
                id: 4,
                text: 'Dynamic DOM.',
            },
        ],
    },
};

export default question;