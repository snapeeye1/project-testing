const question = {
    en: {
        question: 'Does Stateless Functional Components in React.js invoke life cycle methods?',
        tip: 'Stateless Functional Components have rather low functionality.',
        correctId: 1,
        options: [
            {
                id: 1,
                text: 'No.',
            },
            {
                id: 2,
                text: 'Yes.',
            },
            {
                id: 3,
                text: 'Yes, using third-party library APIs.',
            },
            {
                id: 4,
                text: 'Yes, wrapping the content in the <React.StatelessLifecycle> block.',
            },
        ],
    },
    ru: {
        question: 'Вызываются ли у Stateless Functional Components в React.js методы жизненного цикла?',
        tip: 'Stateless Functional Components имеют достаточно сжатый функционал.',
        correctId: 1,
        options: [
            {
                id: 1,
                text: 'Нет.',
            },
            {
                id: 2,
                text: 'Да.',
            },
            {
                id: 3,
                text: 'Да, используя API сторонних библиотек.',
            },
            {
                id: 4,
                text: 'Да, обернув контент в <React.StatelessLifecycle> блок.',
            },
        ],
    },
};

export default question;