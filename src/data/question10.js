const question = {
    en: {
        question: 'What is React?',
        tip: 'This is something set by the npm i command.',
        correctId: 1,
        options: [
            {
                id: 1,
                text: 'Library.',
            },
            {
                id: 2,
                text: 'Framework.',
            },
            {
                id: 3,
                text: 'Markup API.',
            },
            {
                id: 4,
                text: 'None of the above.',
            },
        ],
    },
    ru: {
        question: 'Что такое React?',
        tip: 'Это что-то устанавливается командой npm i.',
        correctId: 1,
        options: [
            {
                id: 1,
                text: 'Библиотека.',
            },
            {
                id: 2,
                text: 'Фреймворк.',
            },
            {
                id: 3,
                text: 'API разметки.',
            },
            {
                id: 4,
                text: 'Ничего из вышеперечисленного.',
            },
        ],
    },
};

export default question;