const question = {
    en: {
        question: 'With what construct, you can perform a render of a child component outside its DOM structure, while maintaining the parent context.',
        tip: 'The child component will be mounted in a separate block, independent of the parent component.',
        correctId: 2,
        options: [
            {
                id: 1,
                text: 'ReactDOM.render()',
            },
            {
                id: 2,
                text: 'React.createPortal()',
            },
            {
                id: 3,
                text: 'React.createContext()',
            },
            {
                id: 4,
                text: 'React.PureComponent',
            },
        ],
    },
    ru: {
        question: 'С помощью какой конструкции можно выполнить render дочернего коммпанента вне его DOM труктуры, сохраняя родительский контекст.',
        tip: 'Дочерний компанент будет смонтирован в отдельном блоке, не зависящем от родительского компанента.',
        correctId: 2,
        options: [
            {
                id: 1,
                text: 'ReactDOM.render()',
            },
            {
                id: 2,
                text: 'React.createPortal()',
            },
            {
                id: 3,
                text: 'React.createContext()',
            },
            {
                id: 4,
                text: 'React.PureComponent',
            },
        ],
    },
};

export default question;