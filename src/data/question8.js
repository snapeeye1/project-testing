const question = {
    en: {
        question: 'What lifecycle method can disable/enable component redrawing?',
        tip: 'The method decides whether to update the component as a whole.',
        correctId: 3,
        options: [
            {
                id: 1,
                text: 'getDerivedStateFromProps()',
            },
            {
                id: 2,
                text: 'componentWillUpdate()',
            },
            {
                id: 3,
                text: 'shouldComponentUpdate()',
            },
            {
                id: 4,
                text: 'componentDidUpdate()',
            },
        ],
    },
    ru: {
        question: 'Какой метод жизненного цикла может запретить/разрешить перерисовку компанента?',
        tip: 'Метод решает следует ли обновлять компанент в целом.',
        correctId: 3,
        options: [
            {
                id: 1,
                text: 'getDerivedStateFromProps()',
            },
            {
                id: 2,
                text: 'componentWillUpdate()',
            },
            {
                id: 3,
                text: 'shouldComponentUpdate()',
            },
            {
                id: 4,
                text: 'componentDidUpdate()',
            },
        ],
    },
};

export default question;