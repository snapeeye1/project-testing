const question = {
    en: {
        question: 'Choose the correct syntax assignment option in JSX.',
        tip: 'JSX syntax has a non standard way of setting a class parameter.',
        correctId: 0,
        options: [
            {
                id: 1,
                text: '<div className="display-none" />',
            },
            {
                id: 2,
                text: '<div propsClass="display-none" />',
            },
            {
                id: 3,
                text: '<div className={`display-none`} />',
            },
            {
                id: 4,
                text: '<div class={\'display-none\'} />',
            },
        ],
    },
    ru: {
        question: 'Выберите правильный вариант задания класса в JSX.',
        tip: 'JSX синтаксис имеет не стандартный способ задачи параметра класса.',
        correctId: 1,
        options: [
            {
                id: 1,
                text: '<div className="display-none" />',
            },
            {
                id: 2,
                text: '<div propsClass="display-none" />',
            },
            {
                id: 3,
                text: '<div className={`display-none`} />',
            },
            {
                id: 4,
                text: '<div class={\'display-none\'} />',
            },
        ],
    },
};

export default question;