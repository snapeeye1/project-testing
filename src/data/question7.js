const question = {
    en: {
        question: 'Which method call causes a component to change state?',
        tip: 'The set of the specified state should be executed.',
        correctId: 4,
        options: [
            {
                id: 1,
                text: 'changeState()',
            },
            {
                id: 2,
                text: 'putState()',
            },
            {
                id: 3,
                text: 'setStateless()',
            },
            {
                id: 4,
                text: 'None of them.',
            },
        ],
    },
    ru: {
        question: 'Вызов какого метода приводит к изменению состояния компонента?',
        tip: 'Должен выполниться set указанного состояния.',
        correctId: 4,
        options: [
            {
                id: 1,
                text: 'changeState()',
            },
            {
                id: 2,
                text: 'putState()',
            },
            {
                id: 3,
                text: 'setStateless()',
            },
            {
                id: 4,
                text: 'Ни один из них.',
            },
        ],
    },
};

export default question;