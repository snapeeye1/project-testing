const question = {
    en: {
        question: 'As a result of the assignment of which key element will be reduced performance?',
        tip: 'Frequent changes to the key parameter of the companent will result in numerous redrawing.',
        correctId: 1,
        options: [
            {
                id: 1,
                text: 'Math.random()',
            },
            {
                id: 2,
                text: 'index as a result of calling map()',
            },
            {
                id: 3,
                text: 'Unique key, for example, record id from DB.',
            },
            {
                id: 4,
                text: 'Item does not have a key.',
            },
        ],
    },
    ru: {
        question: 'В результате присвоения какого key элементу будет снижен performance?',
        tip: 'Частое изменение параметра key компанента приведёт к многочисленным его перерисовкам.',
        correctId: 1,
        options: [
            {
                id: 1,
                text: 'Math.random()',
            },
            {
                id: 2,
                text: 'index в результате вызова метода map()',
            },
            {
                id: 3,
                text: 'Уникальный key, например, id записи из БД.',
            },
            {
                id: 4,
                text: 'Элемент не имеет key.',
            },
        ],
    },
};

export default question;