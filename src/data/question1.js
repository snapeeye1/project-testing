const question = {
    en: {
        question: 'Is it possible to access the value of the element of the child component through the ref of this element?',
        tip: 'Parent component -> ref -> Child component',
        correctId: 2,
        options: [
            {
                id: 1,
                text: 'No, it is not possible to access the elements of the child component.',
            },
            {
                id: 2,
                text: 'Yes, if you pass the ref value for the child component to props and get to the element using a chain of refs.',
            },
            {
                id: 3,
                text: 'Yes, you can access it using the this keyword.',
            },
            {
                id: 4,
                text: 'Access to child components can be obtained through the context of the parent component.',
            },
        ],
    },
    ru: {
        question: 'Возможно ли получить доступ к значению элемента дочернего компонента через ref этого элемента?',
        tip: 'Родительский компонент -> ref -> Дочерний компонент',
        correctId: 2,
        options: [
            {
                id: 1,
                text: 'Нет, не возможно получить доступ к элементам дочернего компонента.',
            },
            {
                id: 2,
                text: 'Да, если передать в props значение ref для дочернего компонента и добраться до элемента путем цепочки ref-ов.',
            },
            {
                id: 3,
                text: 'Да, можно получить доступ с помощью ключевого слова this.',
            },
            {
                id: 4,
                text: 'Доступ к дочерним компонентам можно получить через context родительского компанента.',
            },
        ],
    },
};

export default question;