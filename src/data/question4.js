const question = {
    en: {
        question: 'Choose the correct definition of inline-styles in JSX.',
        tip: 'The inline-style parameter is waiting for a style configuration object.',
        correctId: 4,
        options: [
            {
                id: 1,
                text: '<div propsStyle={{display: "none"}} />',
            },
            {
                id: 2,
                text: '<div styleNames="display: none;" />',
            },
            {
                id: 3,
                text: '<div style={display: "none"} />',
            },
            {
                id: 4,
                text: '<div style={{display: "none"}} />',
            },
        ],
    },
    ru: {
        question: 'Выберите правильный вариант определения inline-стилей в JSX.',
        tip: 'Параметр inline-стилей ожидает объект конфигураций стилей.',
        correctId: 4,
        options: [
            {
                id: 1,
                text: '<div propsStyle={{display: "none"}} />',
            },
            {
                id: 2,
                text: '<div styleNames="display: none;" />',
            },
            {
                id: 3,
                text: '<div style={display: "none"} />',
            },
            {
                id: 4,
                text: '<div style={{display: "none"}} />',
            },
        ],
    },
};

export default question;