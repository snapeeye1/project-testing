import ReactDOM from "react-dom";
import React from "react";
import en from "../lang/en";
import ru from "../lang/ru";

export function getCookie (name) {
    const matches = document.cookie.match(new RegExp(
        "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
    ));
    return matches ? decodeURIComponent(matches[1]) : null;
};

export function getLastLanguage () {
    return getCookie('lastLang') || 'en';
};

export function getLanguageResources () {
    return {
        en,
        ru,
    };
};

export function getLangOptions () {
    return [
        { value: 'en', label: 'EN' },
        { value: 'ru', label: 'RU' },
    ];
};

export function initialize (InitComponent, beforeInitCallback) {
    let customProps = {};
    if (beforeInitCallback && typeof beforeInitCallback === 'function') {
        customProps = beforeInitCallback();
    }

    const lastLang = getLastLanguage();
    const langOptions = getLangOptions();
    const resources = getLanguageResources();
    ReactDOM.render(<InitComponent lang={ lastLang } langOptions={ langOptions } resources={ resources } { ...customProps } />, document.getElementById('root'));
};