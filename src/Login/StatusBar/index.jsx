import React from 'react';
import './StatusBar.less';

export default class StatusBar extends React.PureComponent {
    render() {
        const { status, isErrorStatus } = this.props;
        const textClass = isErrorStatus ? 'red' : 'green';

        return (
            <React.Fragment>
                <span className={`status-bar__text ${ textClass }`}>{ status }</span>
            </React.Fragment>
        );
    }
}