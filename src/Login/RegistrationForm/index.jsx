import React from 'react';
import './RegistrationForm.less';

export default class RegistrationForm extends React.PureComponent {
    registration = () => {
        const { registerEmail, registerPassword, registerNickname } = this.props;
        const xmlHttp = new XMLHttpRequest();
        xmlHttp.open("POST", 'http://localhost:3000/registration', true);
        xmlHttp.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
        xmlHttp.onreadystatechange = () => {
            this.parseRegistrationResponse(xmlHttp);
        };
        const params = JSON.stringify({
            email: registerEmail,
            password: registerPassword,
            nickname: registerNickname,
        });
        xmlHttp.send(params);
    };

    parseRegistrationResponse = (xmlHttp) => {
        const { onChangeStatus } = this.props;

        if (xmlHttp.readyState === 4) {
            let response = {};
            try {
                response = JSON.parse(xmlHttp.response);
            } catch (e) {
                onChangeStatus('serverParseError', true);
                return;
            }

            if (xmlHttp.status === 200) {
                onChangeStatus(response.status, false);
            } else if (xmlHttp.status === 400) {
                onChangeStatus(response.status, true);
            } else {
                onChangeStatus('serverUnknownError', true);
            }
        }
    };

    render() {
        const { registration } = this;
        const { resources, onInputChange, registerEmail, registerPassword, registerNickname } = this.props;

        return (
            <div className={'auth-forms__register-form register-form'}>
                <div className={'register-form__email-input email-input'}>
                    <div className={'email-input__text'}>{ resources.loginPageEmailInputText }</div>
                    <input id={'registerEmail'} className={'email-input__input'} type={'email'} value={ registerEmail } onChange={ onInputChange } />
                </div>
                <div className={'register-form__password-input password-input'}>
                    <div className={'password-input__text'}>{ resources.loginPagePasswordInputText }</div>
                    <input id={'registerPassword'} className={'password-input__input'} type={'password'} value={ registerPassword } onChange={ onInputChange } />
                </div>
                <div className={'register-form__nickname-input nickname-input'}>
                    <div className={'nickname-input__text'}>{ resources.loginPageNicknameInputText }</div>
                    <input id={'registerNickname'} className={'nickname-input__input'} type={'text'} value={ registerNickname } onChange={ onInputChange } />
                </div>
                <div className={'register-form__register-button register-button'}>
                    <button className={'register-button__button'} onClick={ registration }>{ resources.loginPageRegisterButtonText }</button>
                </div>
            </div>
        );
    }
}