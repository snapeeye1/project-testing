import React from 'react';
import ReactDropdown from 'react-dropdown';
import './LoginPage.less';
import LoginForm from '../LoginForm/index.jsx';
import RegistrationForm from '../RegistrationForm/index.jsx';
import StatusBar from '../StatusBar/index.jsx';

export default class LoginPage extends React.Component {
    state = {
        lang: this.props.lang,
        status: 'loginDefaultStatus',
        isErrorStatus: false,
        loginEmail: '',
        loginPassword: '',
        registerEmail: '',
        registerPassword: '',
        registerNickname: '',

    };

    onLangSelect = (selectedValue) => {
        const selectedLang = selectedValue.value;
        document.cookie = "lastLang=" + selectedLang;
        this.setState({
            lang: selectedLang,
        });
    };

    onChangeStatus = (status, isErrorStatus) => {
        this.setState({
            status,
            isErrorStatus,
        })
    };

    onInputChange = (event) => {
        const { id, value } = event.target;
        this.setState({
            [id]: value,
        });
    };

    onAnonymousLogIn = () => {
        window.location.href = 'index.html';
    };

    render() {
        const { onChangeStatus, onInputChange, onAnonymousLogIn } = this;
        const { lang, status, isErrorStatus, loginEmail, loginPassword, registerEmail, registerPassword, registerNickname } = this.state;
        const { langOptions, resources } = this.props;
        const langResources = resources[lang];
        const statusString = langResources[status];

        return (
            <div className={'login-wrapper'}>
                <div className={'login-wrapper__language-dropdown'}>
                    <ReactDropdown
                        className = 'lang-dropdown'
                        controlClassName = 'lang-dropdown-control'
                        menuClassName = 'lang-dropdown-menu'
                        options = {langOptions}
                        onChange = {this.onLangSelect} value={lang} />
                </div>
                <div className={'login-wrapper__auth-forms auth-forms'}>
                    <LoginForm resources = { langResources }
                               onChangeStatus = { onChangeStatus }
                               onInputChange = { onInputChange }
                               loginEmail = { loginEmail }
                               loginPassword = { loginPassword } />
                    <RegistrationForm resources={ langResources }
                                      onChangeStatus = { onChangeStatus }
                                      onInputChange = { onInputChange }
                                      registerEmail = { registerEmail }
                                      registerPassword = { registerPassword }
                                      registerNickname = { registerNickname }/>
                </div>
                <div className={'login-wrapper__anonymous-mode anonymous-mode'}>
                    <button className={'anonymous-mode__button'} onClick={ onAnonymousLogIn }>{ langResources.loginAnonymousButtonText }</button>
                </div>
                <div className={'login-wrapper__status-bar status-bar'}>
                    <StatusBar status = { statusString }
                               isErrorStatus = { isErrorStatus } />
                </div>
            </div>
        );
    }
}