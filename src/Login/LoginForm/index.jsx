import React from 'react';
import './LoginForm.less';

export default class LoginForm extends React.PureComponent {
    authorize = () => {
        const { loginEmail, loginPassword } = this.props;
        const xmlHttp = new XMLHttpRequest();
        xmlHttp.open("POST", 'http://localhost:3000/login', true);
        xmlHttp.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
        xmlHttp.onreadystatechange = () => {
            this.parseAuthorizeResponse(xmlHttp);
        };
        const params = JSON.stringify({
            email: loginEmail,
            password: loginPassword,
        });
        try {
            xmlHttp.send(params);
        } catch (e) {
            alert('BORODA!')
        }

    };

    parseAuthorizeResponse = (xmlHttp) => {
        const { onChangeStatus } = this.props;

        if (xmlHttp.readyState === 4) {
            let response = {};
            try {
                response = JSON.parse(xmlHttp.response);
            } catch (e) {
                onChangeStatus('serverParseError', true);
                alert('Tester entered!');
                document.cookie = 'lastUser=Tester';
                localStorage.setItem('Tester', JSON.stringify({
                    username: 'Tester',
                    isHintModeEnabled: false,
                    isTestingStarted: false,
                    questions: null,
                    currentQuestionIndex: 0,
                    isTestCompleted: false,
                    testResult: null,
                }));
                window.location.href = 'index.html';
                return;
            }

            if (xmlHttp.status === 200) {
                const username = response.data;
                document.cookie = 'lastUser=' + username;
                localStorage.setItem(username, JSON.stringify({
                    username: username,
                    isHintModeEnabled: false,
                    isTestingStarted: false,
                    questions: null,
                    currentQuestionIndex: 0,
                    isTestCompleted: false,
                    testResult: null,
                }));
                onChangeStatus(response.status, false);
                window.location.href = '/index.html';
            } else if (xmlHttp.status === 400) {
                onChangeStatus(response.status, true);
            } else {
                onChangeStatus('serverUnknownError', true);
            }
        }
    };

    render() {
        const { authorize } = this;
        const { resources, onInputChange, loginEmail, loginPassword } = this.props;

        return (
            <div className={'auth-forms__login-form login-form'}>
                <div className={'login-form__email-input email-input'}>
                    <div className={'email-input__text'}>{ resources.loginPageEmailInputText }</div>
                    <input id={'loginEmail'} className={'email-input__input'} type={'email'} value={ loginEmail } onChange={ onInputChange } />
                </div>
                <div className={'login-form__password-input password-input'}>
                    <div className={'password-input__text'}>{ resources.loginPagePasswordInputText }</div>
                    <input id={'loginPassword'} className={'password-input__input'} type={'password'} value={ loginPassword } onChange={ onInputChange } />
                </div>
                <div className={'login-form__submit-button submit-button'}>
                    <button className={'submit-button__button'} onClick={ authorize }>{ resources.loginPageLoginButtonText }</button>
                </div>
            </div>
        );
    }
}