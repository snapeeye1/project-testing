import React from 'react';
import './index.less';
import { initialize } from "../utils/helpers";
import LoginPage from './LoginPage/index.jsx';

initialize(LoginPage, null);