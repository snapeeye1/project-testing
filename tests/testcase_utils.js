// import { getCookie, getLastLanguage, getLanguageResources, getLangOptions, initialize } from '../src/utils/helpers';
import { getLangOptions } from '../src/utils/helpers';
import { assert } from 'chai';

describe('getLangOptions()', () => {
    it('Should return correct lang array of objects with specified locales', () => {
        const actual = getLangOptions();
        const expected = [
            { value: 'en', label: 'EN' },
            { value: 'ru', label: 'RU' },
        ];
        assert.deepStrictEqual(actual, expected);
    });
});