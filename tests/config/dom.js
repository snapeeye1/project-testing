const jsdom = require("jsdom");
const {JSDOM} = jsdom;
const {window} = new JSDOM(`<!doctype html><html><head></head><body><div id="root"></div></body></html>`);
global.localStorage = require('localStorage');
global.window = window;
global.document = global.window.document;
global.navigator = {userAgent: 'node.js'};

module.exports.createTestMockData = function (mockProps) {
    if (mockProps.query) {
        $.QueryString = {...mockProps.query};
        if (mockProps.query.tid) {
            global.localStorage.setItem('_session_' + mockProps.query.tid, '{"su":123,"sp":321,"lang":"ar"}');
        }
    }
    if (mockProps.navigator) {
        global.navigator = {...navigator, ...mockProps.navigator};
    }
    if (mockProps.global) {
        for (let prop in mockProps.global) {
            global[prop] = mockProps.global[prop];
        }

        if (typeof mockProps.global.tid === 'number') {
            global.localStorage.setItem('_session_' + tid, '{"su":123,"sp":321,"lang":"en"}');
        }
    }
    if (mockProps.document) {
        for (let prop in mockProps.document) {
            document[prop] = mockProps.document[prop];
        }
    }
};